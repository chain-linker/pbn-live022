---
layout: post
title: "성은정 “입스 두려움 지워가는 중…리디아 언니처럼 멋지게 부활해야죠”"
toc: true
---

 성은정, KLPGA 투어 NH투자증권 레이디스 출전
티샷 입스 극복하기 위해 피나는 노력
리디아 고·조던 스피스, 화려한 갱생 큰 동기부여
"티샷 자부 찾아가는 중…곧 극복할거라 믿어"

 부활을 꿈꾸는 성은정(22)이 하루에 장부 듬뿍 떠올리는 단어는 ‘골프’다. 그는 13일 이데일리와 만나 “어제도 임성재와 안병훈 선배에게 어떻게 하면 드라이버를 올바로 칠 복 있을지 물어보는 꿈을 꿨다”며 “24시간 중앙 12시간 이상을 골프만 생각할 정도로 골프에 파묻혀 살고 있다”고 말했다.
성은정은 아마추어 날 US 요조숙녀 주니어챔피언십, US 계집 아마추어선수권을 제패하는 등 맹활약을 펼치며 최혜진(22)과 나란히 한국 여자골프를 이끌어갈 기대주로 주목받았다. 그러나 성은정은 티샷 입스(Yips)에 시달리며 기대만큼 성장하지 못했고 시방 한국여자프로골프(KLPGA) 투어의 2부 격인 드림 투어에서 뛰고 있다.
입스는 샷을 하기 전 실패의 두려움 왜냐하면 불안한 상태에 빠지는 걸 의미한다. 정상적인 스윙을 하지 못하게 만들어 골프 선수에게 치명적이다. 2017년 티샷 입스가 시작된 성은정은 5년째 티샷 자신감을 찾기 위해 피나는 노력을 하고 있다.
드로(공이 낙하하면서 왼쪽으로 떨어지는 구질)를 기본 구질로 쳤던 성은정은 처음에는 티샷에 [골프입스](https://lunchfar.com/sports/post-00028.html){:rel="nofollow"} 대한 부담감이 크지 않았다. 왼쪽으로 감기는 실수만 조심하면 됐던 만큼 몸소 있게 티샷을 날렸다. 그렇지만 오른쪽으로 공이 밀리는 실수가 반복되면서 성은정의 티샷 입스가 시작됐다. 그는 한쪽이 아닌 양쪽으로 실수를 기예 위해 다양한 시도를 했지만 쉽게 극복하지 못했다. 나가는 대회마다 티샷 실수에 발목을 잡혔고 팬들의 견해 속에서도 사라졌다.
그는 “티샷 미스가 양쪽으로 나기 시작하면서부터 입스가 시작됐다. 안해 겨울까지만 해도 티잉 그라운드에 서면 불안하다는 생각이 들판 정도로 실태 힘든 시간을 보냈다”며 “여전히 마음 한켠에 불안함이 자리하고 있지만 예년보다 또렷이 마음이 편해졌다. 잃어버렸던 자신감을 찾아가고 있는 만큼 올해가 가기 전에는 티샷 입스에서 벗어날 운명 있을 것이라고 믿는다”고 말했다.
생각의 전환도 성은정이 티샷에 자신감을 찾는 데 한몫했다. 그는 “지금은 코스에서 100%로 스윙하고 있다. 맞춰서 쳐야지만 공이 곧 간다는 생각을 바꿨다”며 “또 공이 원하는 곳으로 분지 않아도 된다는 마음으로 샷을 하고 있다. 두려움에 맞서 싸우면서 점점 티샷에 대한 두려움이 사라지고 있다”고 말했다.
성은정은 이사이 입스를 탈출한 선수들의 기사와 인터뷰도 찾아보고 있다. 그는 “지난해 KPGA 코리안투어 대상과 상금왕을 차지한 김태훈 프로님의 입스 극복 기사를 보고 직접 물어보고 싶다는 생각이 들었다”며 “다른 선수들이 입스를 이겨낸 대미 우승을 차지하는 걸 보면 나도 할 요체 있다는 자신감이 생긴다”고 설명했다.
지난달 미국여자프로골프(LPGA) 투어 롯데 챔피언십에서 1084일 만에 정상에 오른 리디아 고(뉴질랜드)와 미국프로골프(PGA) 투어 발레로 텍사스 오픈에서 3년 9개월 만에 우승의 감격을 맛본 조던 스피스(미국)의 부활도 성은정에게 큰 동기부여가 됐다.
그는 “리디아 고통 언니와 스피스의 경기를 보며 감동을 받았다”며 “리디아 괴로움 언니가 이사이 ‘너도 할 수 있다’고 자신감을 불어 넣어줘 진품 감사했다. 리디아 고와 스피스처럼 나도 부활이라는 단어가 어울리는 선수가 될 요행 있도록 노력하겠다”고 말했다.
성은정은 퍼센트 데뷔 뒤 처녀 출전하는 KLPGA 투어 대회에 임하는 각오도 전했다. 그는 14일부터 나흘간 경기도 용인시 수원 컨트리클럽에서 열리는 KLPGA 투어 NH투자증권 레이디스 챔피언십(총상금 7억원)에 추천 선수로 출전한다.
그는 “19살 이후 정규투어 대회에 초야 나가는 만큼 정작 매우 떨린다”며 “이번 집합 목표는 내가 만족하는 골프를 하는 것이다. 그동안 내가 준비했던 노력의 힘을 믿고 바지런스레 쳐보겠다”고 말했다.
성은정은 골프 선수 생활을 마치기 전 US여자오픈에서 우승하고 싶다는 바람도 전했다. 그는 “US여자오픈 우승은 어린 시절부터 간직했던 꿈”이라며 “40살까지 선수 생활을 하는 걸 목표로 하고 있는데 그전까지 필연 제한 제차 US여자오픈 정상에 오르는 감격을 맛보겠다”고 강조했다.
